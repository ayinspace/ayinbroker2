package space.ayin.broker;

import com.google.inject.Injector;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import space.ayin.lang.ayin.AyinStandaloneSetup;
import space.ayin.lang.ayin.ayin.AyinEntity;
import space.ayin.lang.ayin.ayin.AyinPackage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;


public class AyinResourceUtils {


    private volatile Resource ayinRuntimeResource;

    private volatile Resource ayinProgramResource;

    private volatile Resource ayinRuntimeTmpResource;

    private volatile XtextResourceSet resourceSet;

    private volatile long i = 0;


    public AyinResourceUtils() {
        Injector injector = new AyinStandaloneSetup().createInjectorAndDoEMFRegistration();
        resourceSet = injector.getInstance(XtextResourceSet.class);

        resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);

        // Register the appropriate ayinRuntimeResource factory to handle all file extensions.
        //

        //resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("ayinruntime", new XMIResourceFactoryImpl());

        registerPackage(AyinPackage.eNS_URI,
                AyinPackage.eINSTANCE);

//        registerPackage(AyinruntimePackage.eINSTANCE.eNS_URI,
//                AyinruntimePackage.eINSTANCE);


        ayinRuntimeResource = resourceSet.createResource(URI.createURI("program.ayinruntime"));

//        if (ayinRuntimeResource.getContents().isEmpty()) {
//            ayinRuntimeResource.getContents().add(AyinruntimeFactory.eINSTANCE.createAyinRuntimeDomain());
//        }

    }

    public synchronized void registerPackage(String nsUri, EPackage ePackage) {
        resourceSet.getPackageRegistry().put(nsUri, ePackage);
    }

    /*public synchronized void loadAyinProgram(URI uri) throws IOException {
        ayinProgramResource = resourceSet.createResource(uri);
        ayinProgramResource.load(resourceSet.getLoadOptions());
        //ayinProgramResource = resourceSet.createResource(URI.createURI("http:///program.ayin"));
        //TODO: validate
        for (AyinAbstractElement elem : ((AyinProgram)ayinProgramResource.getContents().get(0)).getElements()) {
            if (elem instanceof AyinExecutor) {
                executors.put(elem.getName(), (AyinExecutor) elem);
            }
        }
        getRuntimeDomain().getPrograms().add((AyinProgram) ayinProgramResource.getContents().get(0));
    }*/



    /*public synchronized AyinProgram getAyinProgram() {
        return getRuntimeDomain().getPrograms().get(0);
    }*/

    /*private synchronized Resource getRuntimeResource() {
        i++;
        if (ayinRuntimeTmpResource == null) {
            ayinRuntimeTmpResource = resourceSet.createResource(URI.createURI("tmp" + i + ".ayinruntime"));
        } else {
            ayinRuntimeTmpResource.unload();
            resourceSet.getResources().remove(ayinRuntimeTmpResource);
            ayinRuntimeTmpResource = resourceSet.createResource(URI.createURI("tmp" + i + ".ayinruntime"));
        }
        return ayinRuntimeTmpResource;
    }*/


    private synchronized Resource getAyinTmpResource() {
        i++;
        if (ayinRuntimeTmpResource == null) {
            ayinRuntimeTmpResource = resourceSet.createResource(URI.createURI("tmp" + i + ".ayin"));
        } else {
            ayinRuntimeTmpResource.unload();
            resourceSet.getResources().remove(ayinRuntimeTmpResource);
            ayinRuntimeTmpResource = resourceSet.createResource(URI.createURI("tmp" + i + ".ayin"));
        }
        return ayinRuntimeTmpResource;
    }

    public synchronized AyinEntity deserializeAyin(ByteArrayInputStream inputStream) throws IOException {
        Resource res = getAyinTmpResource();
        res.load(inputStream, Collections.EMPTY_MAP);
        EObject result = EcoreUtil.copy(res.getContents().get(0));
        resourceSet.getResources().remove(res);
        return (AyinEntity) result;
    }

    /*public synchronized String serialize(EObject object) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Resource res = getRuntimeResource();
        res.getContents().add(object);
        res.save(outputStream, Collections.EMPTY_MAP);
        String result = outputStream.toString();
        resourceSet.getResources().remove(res);
        return result;
    }

    public synchronized EObject deserialize(ByteArrayInputStream inputStream) throws IOException {
        Resource res = getRuntimeResource();
        res.load(inputStream, Collections.EMPTY_MAP);
        EObject result = EcoreUtil.copy(res.getContents().get(0));
        resourceSet.getResources().remove(res);
        return result;
    }

    public synchronized EObject deserialize(String s) throws IOException {
        ByteArrayInputStream is = new ByteArrayInputStream(s.getBytes());
        return deserialize(is);
    }*/

    public synchronized AyinEntity deserializeAyin(String s) throws IOException {
        ByteArrayInputStream is = new ByteArrayInputStream(s.getBytes());
        return deserializeAyin(is);
    }



}
