package space.ayin.broker;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.WindowEvent;
import org.eclipse.xtext.resource.XtextResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import space.ayin.broker.samplecodegen.ProgramRegistry;
import space.ayin.broker.samplecodegen.ProjectViewTreeItem;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    private static Logger logger = LoggerFactory.getLogger(MainController.class);

    private XtextResource currentResource;

    @FXML
    public TextArea editorTextArea;
    @FXML
    public Button saveButton;
    @FXML
    private TreeView projectTreeView;

    private ProgramRegistry programRegistry = new ProgramRegistry();

    private void createStruct() {
        TextInputDialog structNameDialog = new TextInputDialog();
        Optional<String> result = structNameDialog.showAndWait();
        if (result.isPresent()) {
            logger.info("new struct {}", result.get());
            programRegistry.createAyinStruct(result.get());
        }
    }

    private void createExecutor() {
        TextInputDialog executorNameDialog = new TextInputDialog();
        Optional<String> result = executorNameDialog.showAndWait();
        if (result.isPresent()) {
            logger.info("new executor {}", result.get());
            programRegistry.createAyinExecutor(result.get());
        }
    }


    private void createInterface() {
        TextInputDialog interfaceNameDialog = new TextInputDialog();
        Optional<String> result = interfaceNameDialog.showAndWait();
        if (result.isPresent()) {
            logger.info("new interface {}", result.get());
            programRegistry.createAyinInterface(result.get());
        }
    }


    private void createException() {
        TextInputDialog exceptionNameDialog = new TextInputDialog();
        Optional<String> result = exceptionNameDialog.showAndWait();
        if (result.isPresent()) {
            logger.info("new exception {}", result.get());
            programRegistry.createAyinException(result.get());
        }
    }


    private void createDataType() {
        TextInputDialog dataTypeNameDialog = new TextInputDialog();
        Optional<String> result = dataTypeNameDialog.showAndWait();
        if (result.isPresent()) {
            logger.info("new dataType {}", result.get());
            programRegistry.createAyinData(result.get());
        }
    }


    private void createEnum() {
        TextInputDialog enumNameDialog = new TextInputDialog();
        Optional<String> result = enumNameDialog.showAndWait();
        if (result.isPresent()) {
            logger.info("new enum {}", result.get());
            programRegistry.createAyinEnum(result.get());
        }
    }

    private void updateProjectExplorer() {
        ProjectViewTreeItem projectViewTreeItem = new ProjectViewTreeItem(programRegistry.getAyinLangResourceSet());
        projectTreeView.setRoot(projectViewTreeItem);
        projectViewTreeItem.setExpanded(true);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        final ContextMenu contextMenu = new ContextMenu();
        contextMenu.setOnShowing(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent e) {
//                System.out.println("showing");
            }
        });
        contextMenu.setOnShown(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent e) {
//                System.out.println("shown");
            }
        });

        MenuItem newStructMenuItem = new MenuItem("New struct");
        newStructMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                createStruct();
                updateProjectExplorer();
            }
        });

        MenuItem newExecutorMenuItem = new MenuItem("New executor");
        newExecutorMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                createExecutor();
                updateProjectExplorer();
            }
        });


        MenuItem newInterfaceMenuItem = new MenuItem("New interface");
        newInterfaceMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                createInterface();
                updateProjectExplorer();
            }
        });

        MenuItem newDataTypeMenuItem = new MenuItem("New datatype");
        newDataTypeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                createDataType();
                updateProjectExplorer();
            }
        });

        MenuItem newExceptionMenuItem = new MenuItem("New exception");
        newExceptionMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                createException();
                updateProjectExplorer();
            }
        });

        MenuItem newEnumMenuItem = new MenuItem("New enum");
        newEnumMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                createEnum();
                updateProjectExplorer();
            }
        });




        saveButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                 if (currentResource != null) {
                    InputStream inputStream = new ByteArrayInputStream(editorTextArea.getText().getBytes());
                    try {
                        currentResource.unload();
                        currentResource.load(inputStream, Collections.EMPTY_MAP);
                        editorTextArea.setText(programRegistry.serializeAyin(currentResource));
                    } catch (IOException e) {
                        logger.error(e.getMessage(), e);
                    }
                }
            }
        });

        MenuItem openItem = new MenuItem("Open");
        openItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                ProjectViewTreeItem selectedItem = (ProjectViewTreeItem) projectTreeView.getSelectionModel().getSelectedItem();
                if (selectedItem.getProjectItem() instanceof XtextResource) {
                    currentResource = (XtextResource) selectedItem.getProjectItem();
                    editorTextArea.setText(programRegistry.serializeAyin(currentResource));
                }

                //editorTextArea.setText(programRegistry.serializeAyin());
            }
        });
        contextMenu.getItems().addAll(newStructMenuItem, newExecutorMenuItem, newInterfaceMenuItem, newExceptionMenuItem, newEnumMenuItem, newDataTypeMenuItem, openItem);

        projectTreeView.setContextMenu(contextMenu);

        ProjectViewTreeItem projectViewTreeItem = new ProjectViewTreeItem(programRegistry.getAyinLangResourceSet());


        projectTreeView.setRoot(projectViewTreeItem);



    }
}
