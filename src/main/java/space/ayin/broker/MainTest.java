package space.ayin.broker;

import com.google.inject.Injector;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.*;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import space.ayin.broker.codegen.MyEMFGenerator;
import space.ayin.broker.codegen.ProjectResource;
import space.ayin.broker.codegen.ProjectResourceGenerator;
import space.ayin.broker.samplecodegen.ProgramRegistry;
import space.ayin.lang.ayin.AyinStandaloneSetup;
import space.ayin.lang.ayin.ayin.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class MainTest extends Application {

    private static Logger logger = LoggerFactory.getLogger(MainTest.class);


    /*public static synchronized AyinEntity deserializeAyin(String s) throws IOException {
        ByteArrayInputStream is = new ByteArrayInputStream(s.getBytes());
        Resource res = xTextResourceSet.createResource(URI.createURI("tmp" + resCalc + ".ayin"));;
        res.load(is, Collections.EMPTY_MAP);
        //EObject result = EcoreUtil.copy(res.getContents().get(0));
        //resourceSet.getResources().remove(res);
        resCalc++;
        return (AyinEntity) res.getContents().get(0);
    }


    private static EClassifier findEClassifierByName(EPackage ePackage, String name) {
        for (EClassifier eClassifier : ePackage.getEClassifiers()) {
            if (eClassifier.getName().equals(name)) {
                return eClassifier;
            }
        }
        throw new RuntimeException("eClassifier " + name + " not found");
    }

    private static EClassifier getEClassifierByAyinType(EPackage ePackage, AyinType ayinType) {
        if (ayinType instanceof AyinPrimitiveType) {
            if (ayinType.getName().equals("string")) {
                return EcorePackage.Literals.ESTRING;
            } else if (ayinType.getName().equals("i8")) {
                return EcorePackage.Literals.EBYTE;
            } else if (ayinType.getName().equals("i16")) {
                return EcorePackage.Literals.ESHORT;
            } else if (ayinType.getName().equals("i32")) {
                return EcorePackage.Literals.EINT;
            } else if (ayinType.getName().equals("i64")) {
                return EcorePackage.Literals.ELONG;
            } else if (ayinType.getName().equals("float")) {
                return EcorePackage.Literals.EFLOAT;
            } else if (ayinType.getName().equals("double")) {
                return EcorePackage.Literals.EDOUBLE;
            } else if (ayinType.getName().equals("boolean")) {
                return EcorePackage.Literals.EBOOLEAN;
            } else {
                throw new RuntimeException("unknown primitive type " + ayinType.getName());
            }

        } else if (ayinType instanceof AyinEntityType) {
            return findEClassifierByName(ePackage, ((AyinEntityType) ayinType).getEntity().getName());
        }

        throw new RuntimeException("unknown type " + ayinType);
    }*/

    public static void main(String[] args) throws IOException {

        launch(args);



        //deserializeAyin("struct Organization { string name; Person owner; };");
        //deserializeAyin("struct Person { string name; i32 age; Organization organization; };");



        /*EPackage ayinProgramEPackage = EcoreFactory.eINSTANCE.createEPackage();
        ayinProgramEPackage.setName("AyinProgramPackage");
        ayinProgramEPackage.setNsPrefix("ayinProgram");
        ayinProgramEPackage.setNsURI("http:///sample.ayin.space.ecore");


        for (Resource resource : xTextResourceSet.getResources()) {
            if (resource.getContents().get(0) instanceof AyinStruct) {
                AyinStruct struct = (AyinStruct) resource.getContents().get(0);
                EClass eClass = EcoreFactory.eINSTANCE.createEClass();
                eClass.setName(struct.getName());

                ayinProgramEPackage.getEClassifiers().add(eClass);
             }

        }

        for (Resource resource : xTextResourceSet.getResources()) {
            if (resource.getContents().get(0) instanceof AyinStruct) {
                AyinStruct struct = (AyinStruct) resource.getContents().get(0);
                EClass eClass = (EClass) findEClassifierByName(ayinProgramEPackage, struct.getName());
                eClass.setName(struct.getName());
                for (AyinParameter ayinParameter : struct.getFields()) {
                    EAttribute eAttribute = EcoreFactory.eINSTANCE.createEAttribute();
                    eAttribute.setName(ayinParameter.getName());
                    eAttribute.setEType(getEClassifierByAyinType(ayinProgramEPackage, ayinParameter.getType()));

                    eClass.getEStructuralFeatures().add(eAttribute);
                }
                ayinProgramEPackage.getEClassifiers().add(eClass);
            }

        }

        Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put("ecore", new XMIResourceFactoryImpl());


        // create a resource
        Resource resource = resSet.createResource(URI
                .createURI("sampleayinspace.ecore"));
        resource.getContents().add(ayinProgramEPackage);

        // now save the content.
        try {
            resource.save(Collections.EMPTY_MAP);
        } catch (IOException e) {
            e.printStackTrace();
        }


        System.out.println();

        //generator.getResourcesForAyinStruct()
        */
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("AyinBroker");

        Parent root = FXMLLoader.load(getClass().getResource("/scene.fxml"));


        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });
    }
}
