package space.ayin.broker.codegen;

public abstract class SourceFile implements ProjectResource {

    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
