package space.ayin.broker.samplecodegen;

import com.google.inject.Injector;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.formatting2.IFormattableDocument;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import space.ayin.lang.ayin.AyinStandaloneSetup;
import space.ayin.lang.ayin.ayin.*;
import space.ayin.lang.ayin.formatting2.AyinFormatter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;

public class ProgramRegistry {

    private static Logger logger = LoggerFactory.getLogger(ProgramRegistry.class);

    private Injector injector;

    private XtextResourceSet ayinLangResourceSet;

    private ResourceSet resSet;

    private AyinFactory ayinFactory = AyinFactory.eINSTANCE;

    private AyinFormatter ayinFormatter = new AyinFormatter();

    public ProgramRegistry() {
        resSet = new ResourceSetImpl();

        injector = new AyinStandaloneSetup().createInjectorAndDoEMFRegistration();

        ayinLangResourceSet = injector.getInstance(XtextResourceSet.class);

        ayinLangResourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);

        ayinLangResourceSet.getPackageRegistry().put(AyinPackage.eNS_URI, AyinPackage.eINSTANCE);

    }

    public XtextResource createAyinStruct(String name) {
        AyinStruct ayinStruct = ayinFactory.createAyinStruct();
        ayinStruct.setName(name);

        XtextResource resource = (XtextResource) ayinLangResourceSet.createResource(URI.createURI(name + ".ayin"));

        resource.getContents().add(ayinStruct);

        return resource;
    }


    public XtextResource createAyinEnum(String name) {
        AyinEnum ayinEnum = ayinFactory.createAyinEnum();
        ayinEnum.setName(name);

        XtextResource resource = (XtextResource) ayinLangResourceSet.createResource(URI.createURI(name + ".ayin"));

        resource.getContents().add(ayinEnum);

        return resource;
    }


    public XtextResource createAyinData(String name) {
        AyinData ayinData = ayinFactory.createAyinData();
        ayinData.setName(name);
        
        XtextResource resource = (XtextResource) ayinLangResourceSet.createResource(URI.createURI(name + ".ayin"));

        resource.getContents().add(ayinData);

        return resource;
    }


    public XtextResource createAyinInterface(String name) {
        AyinInterface ayinInterface = ayinFactory.createAyinInterface();
        ayinInterface.setName(name);

        XtextResource resource = (XtextResource) ayinLangResourceSet.createResource(URI.createURI(name + ".ayin"));

        resource.getContents().add(ayinInterface);

        return resource;
    }


    public XtextResource createAyinExecutor(String name) {
        AyinExecutor ayinExecutor = ayinFactory.createAyinExecutor();
        ayinExecutor.setName(name);

        XtextResource resource = (XtextResource) ayinLangResourceSet.createResource(URI.createURI(name + ".ayin"));

        resource.getContents().add(ayinExecutor);

        return resource;
    }


    public XtextResource createAyinException(String name) {
        AyinException ayinException = ayinFactory.createAyinException();
        ayinException.setName(name);

        XtextResource resource = (XtextResource) ayinLangResourceSet.createResource(URI.createURI(name + ".ayin"));

        resource.getContents().add(ayinException);

        return resource;
    }
    
    
    
    

    public String serializeAyin(XtextResource xtextResource) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            xtextResource.doSave(byteArrayOutputStream, Collections.EMPTY_MAP);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(byteArrayOutputStream.toByteArray());
    }

    @Override
    public String toString() {
        return "programRegistry";
    }

    public XtextResourceSet getAyinLangResourceSet() {
        return ayinLangResourceSet;
    }

}
