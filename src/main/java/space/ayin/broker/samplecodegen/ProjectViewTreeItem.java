package space.ayin.broker.samplecodegen;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.resource.XtextResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import space.ayin.broker.MainController;
import space.ayin.lang.ayin.ayin.AyinDataType;
import space.ayin.lang.ayin.ayin.AyinEntity;
import space.ayin.lang.ayin.ayin.AyinExecutor;
import space.ayin.lang.ayin.ayin.AyinStruct;

public class ProjectViewTreeItem extends TreeItem<Object> {

    private static Logger logger = LoggerFactory.getLogger(ProjectViewTreeItem.class);

    private Object projectItem;

    public ProjectViewTreeItem(Object projectItem) {
        this.projectItem = projectItem;
        if (projectItem instanceof EObject) {
            setValue("eobj");
        } else if (projectItem instanceof ResourceSet) {
            setValue("resset");
        } else if (projectItem instanceof XtextResource) {
            EObject eObject = ((XtextResource) projectItem).getContents().get(0);
            if (eObject instanceof AyinEntity) {
                setValue(eObject.eClass().getName() + " " + ((AyinEntity) eObject).getName());
            } else {
                setValue("eobj " + eObject);
            }

        } else {
            setValue("unknown " + projectItem);
        }


    }

    @Override
    public ObservableList<TreeItem<Object>> getChildren() {
        if (projectItem instanceof EObject) {
            logger.warn("eobject");
        } else if (projectItem instanceof ResourceSet) {
            ResourceSet resourceSet = (ResourceSet) projectItem;
            ObservableList<TreeItem<Object>> resourcesList = FXCollections.observableArrayList();
            for (Resource resource : resourceSet.getResources()) {
                resourcesList.add(new ProjectViewTreeItem(resource));
            }
            return resourcesList;
        } else if (projectItem instanceof Resource) {
            logger.warn("resource");
        }
        logger.warn("else");
        return super.getChildren();
    }

    @Override
    public boolean isLeaf() {
        return false;
    }

    public Object getProjectItem() {
        return projectItem;
    }
}
